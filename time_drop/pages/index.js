import { Heading, Page } from "@shopify/polaris";
import ResourceList from "./components/ResourceList";

const Index = () => <ResourceList />;

export default Index;
