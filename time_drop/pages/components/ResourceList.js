import React, { useState, useEffect, useMemo, forwardRef } from "react";

import gql from "graphql-tag";
import { useQuery, useMutation } from "react-apollo";
import moment from "moment";

import MaterialTable from "material-table";

import AddBox from "@material-ui/icons/AddBox";
import ArrowUpward from "@material-ui/icons/ArrowUpward";
import Check from "@material-ui/icons/Check";
import ChevronLeft from "@material-ui/icons/ChevronLeft";
import ChevronRight from "@material-ui/icons/ChevronRight";
import Clear from "@material-ui/icons/Clear";
import DeleteOutline from "@material-ui/icons/DeleteOutline";
import Edit from "@material-ui/icons/Edit";
import FilterList from "@material-ui/icons/FilterList";
import FirstPage from "@material-ui/icons/FirstPage";
import LastPage from "@material-ui/icons/LastPage";
import Remove from "@material-ui/icons/Remove";
import SaveAlt from "@material-ui/icons/SaveAlt";
import Search from "@material-ui/icons/Search";
import ViewColumn from "@material-ui/icons/ViewColumn";

const tableIcons = {
  Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
  Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
  Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
  DetailPanel: forwardRef((props, ref) => (
    <ChevronRight {...props} ref={ref} />
  )),
  Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
  Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
  Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
  FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
  LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
  NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
  PreviousPage: forwardRef((props, ref) => (
    <ChevronLeft {...props} ref={ref} />
  )),
  ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
  SortArrow: forwardRef((props, ref) => <ArrowUpward {...props} ref={ref} />),
  ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
  ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
};

const GET_PRODUCTS = gql`
  query getProducts {
    products(first: 30) {
      edges {
        node {
          id
          title
          publishedAt
          featuredImage {
            id
          }
          images(first: 5) {
            edges {
              node {
                id
                altText
              }
            }
          }
          variants(first: 5) {
            edges {
              node {
                displayName
                title
                price
                id
                inventoryQuantity
                inventoryItem {
                  id
                  inventoryLevel(
                    locationId: "gid://shopify/Location/35818340446" # locationId: "gid://shopify/Location/1190494235"
                  ) {
                    id
                  }
                }
              }
            }
          }
        }
      }
    }
  }
`;

// locationId: "gid://shopify/Location/35818340446"
// {
//     "inventoryAdjustQuantityInput" : {
//       "inventoryLevelId": "gid://shopify/InventoryLevel/6485147690?inventory_item_id=12250274365496",
//       "availableDelta": 1
//     }
//   }

const UPDATE_QUANTITY = gql`
  mutation adjustInventoryLevelQuantity(
    $inventoryAdjustQuantityInput: InventoryAdjustQuantityInput!
  ) {
    inventoryAdjustQuantity(input: $inventoryAdjustQuantityInput) {
      inventoryLevel {
        available
      }
      userErrors {
        field
        message
      }
    }
  }
`;

// id: "gid://shopify/Product/4584939913310",
// publishOn: "2020-09-11T14:14:00Z"
const CHANGE_DATE = gql`
  mutation productUpdate($input: ProductInput!) {
    productUpdate(input: $input) {
      product {
        id
        title
      }
      userErrors {
        field
        message
      }
    }
  }
`;

const UPDATE_PRICE = gql`
  mutation productVariantUpdate($input: ProductVariantInput!) {
    productVariantUpdate(input: $input) {
      product {
        id
      }
      productVariant {
        id
        price
      }
      userErrors {
        field
        message
      }
    }
  }
`;
// variables
// {
//   "input": {
//     "id": "gid://shopify/ProductVariant/31365788074006",
//     "price": 10
//   }
// }
const DELETE_VARIANT = gql`
  mutation productVariantDelete($id: ID!) {
    productVariantDelete(id: $id) {
      deletedProductVariantId
      product {
        id
      }
      userErrors {
        field
        message
      }
    }
  }
`;

const DELETE_PRODUCT = gql`
  mutation productDelete($input: ProductDeleteInput!) {
    productDelete(input: $input) {
      userErrors {
        field
        message
      }
      deletedProductId
      shop {
        id
      }
    }
  }
`;
let items = [];
let counter = 0;
function formatDate(time) {
  return moment(new Date(time)).format("MMM D YYYY, h:mm a");
}

let idCounter = 1;
let temp = 1;
let variantCounter;

const format = (data) => {
  console.log("full data", data);
  data.products.edges.map((item) => {
    let variants = item.node.variants.edges;
    let obj = {};
    let time;
    variantCounter = idCounter + 1;
    const { title, publishedAt, id: productId } = item.node;

    if (!publishedAt) {
      time = "";
    } else {
      time = formatDate(publishedAt);
    }

    let obj2 = {};
    obj2.id = idCounter;

    obj2.productId = productId;
    obj2.title = title;
    obj2.price = "";
    obj2.publishedAt = "";
    obj2.inventoryQuantity = 0;
    items.push(obj2);

    for (let variant of variants) {
      let {
        id: variantId,
        inventoryQuantity: variantQty,
        price: variantPrice,
        // title: variantTitle,
        displayName: variantTitle,
      } = variant.node;
      let inventoryId = variant.node.inventoryItem.inventoryLevel.id;

      obj.id = variantCounter;
      variantCounter++;
      obj.productId = variantId;
      obj.title = variantTitle;
      obj.price = parseInt(variantPrice).toFixed(2);
      obj.inventoryQuantity = variantQty;
      obj.inventoryId = inventoryId;
      obj.publishedAt = time;
      obj.parentId = temp;
      items.push(obj);
      obj = {};
    }
    temp = variantCounter;
    idCounter = variantCounter;
  });
  return items;
};

// function makeRowChanges(newData, oldData) {
//   var {
//     price: newPrice,
//     inventoryQuantity: newQty,
//     publishedAt: newDate,
//   } = newData;
//   var {
//     price: oldPrice,
//     inventoryQuantity: oldQty,
//     publishedAt: oldDate,
//   } = oldData;

//   if (newPrice !== oldPrice) {
//     let variables = {
//       input: {
//         id: newData.productVariantId.id,
//         price: newPrice.toString(),
//       },
//     };

//     updatePrice({ variables });
//   }
//   if (newQty !== oldQty) {
//   }
//   if (newDate !== oldDate) {
//   }

//   // console.log('date matched: ')
// }
let onceCounter = 1;

export default function ResourceListWithProducts() {
  const [products, setProducts] = useState([{}]);

  const [updateQuantity] = useMutation(UPDATE_QUANTITY, {
    onError: () => console.log("error!"),
    onCompleted: () => console.log("completed!"),
  });

  const [updatePrice] = useMutation(UPDATE_PRICE, {
    onError: () => console.log("error!"),
    onCompleted: () => console.log("completed!"),
  });

  const [changeDate] = useMutation(CHANGE_DATE, {
    onError: () => console.log("error!"),
    onCompleted: () => console.log("completed!"),
  });

  const [deleteVariant] = useMutation(DELETE_VARIANT, {
    onError: () => console.log("error!"),
    onCompleted: () => console.log("completed!"),
  });

  const [deleteProduct] = useMutation(DELETE_PRODUCT, {
    onError: () => console.log("error!"),
    onCompleted: () => console.log("completed!"),
  });

  function makeRowChanges(newData, oldData) {
    var foundIndex = products.findIndex((i) => i.id === newData.id);

    var {
      productId,
      price: newPrice,
      inventoryQuantity: newQty,
      publishedAt: newDate,
    } = newData;
    var {
      price: oldPrice,
      inventoryQuantity: oldQty,
      publishedAt: oldDate,
    } = oldData;
    console.log(newData);

    if (newPrice !== oldPrice) {
      newPrice = parseFloat(newPrice).toFixed(2);
      products[foundIndex].price = newPrice;
      let variables = {
        input: {
          id: newData.productVariantId.id,
          price: newPrice.toString(),
        },
      };

      updatePrice({ variables });
    }
    console.log("quantity match: ", newQty === oldQty);
    if (newQty !== oldQty) {
      // newValue = parseFloat(newValue);
      products[foundIndex].inventoryQuantity = newPrice;
      let variables = {
        inventoryAdjustQuantityInput: {
          inventoryLevelId: newData.id,
          availableDelta: newQty - oldQty,
        },
      };
      console.log("updated quantity");
      updateQuantity({ variables });
    }
    console.log("date matched: ", newDate === oldDate);
    if (newDate !== oldDate) {
      if (newDate !== "") {
        let time = new Date(newDate);
        // time.setHours(time.getHours() - 7);
        let d = moment(time, "MMM D YYYY, h:mm a").toISOString();

        products[foundIndex].publishedAt = newDate;
        let variables = {
          input: { id: productId, publishOn: d },
        };
        changeDate({ variables });
      } else {
        let variables = {
          input: { id: productId, publishOn: null },
        };
        changeDate({ variables });
      }
    }

    // console.log('date matched: ')
  }

  const { loading, error, data: productData } = useQuery(GET_PRODUCTS);

  // useEffect(() => {
  //   // const getData = () => {

  //   // };

  //   // getData();
  //   if (data) {
  //     console.log("grabbed data ");
  //     setProducts(format(data));
  //   }
  // }, []);
  // useEffect(() => {
  //   const onCompleted = (data) => {
  //     return format(data);
  //   };
  //   const onError = (error) => {
  //     /* magic */
  //   };
  //   console.log("useeffect ", productData);
  //   if (onCompleted || onError) {
  //     if (onCompleted && !loading && !error) {
  //       setProducts(onCompleted(productData));
  //     } else if (onError && !loading && error) {
  //       onError(error);
  //     }
  //   }
  // }, [productData]);

  // let computedData = useMemo(() => {
  //   console.log("memo here");
  //   return products;
  // }, [products]);

  const headers = [
    { title: "Name", field: "title" },
    {
      title: "Price",
      field: "price",
      render: (rowData) => `$${rowData.price}`,
      customSort: (a, b) => parseFloat(a.price) - parseFloat(b.price),
    },
    { title: "Quantity", field: "inventoryQuantity" },
    {
      title: "Status",
      field: "publishedAt",
      type: "datetime",
      render: (rowData) =>
        rowData.publishedAt ? formatDate(rowData.publishedAt) : "Not Set",
      // editable: "always",
    },
  ];

  if (loading) return <div>Loading…</div>;
  if (error) return <div>{error.message}</div>;

  let newProducts = format(productData);
  console.log("counter: ", onceCounter);
  if (onceCounter === 1) {
    setProducts(newProducts);
    onceCounter++;
  }
  console.table(newProducts);
  console.table(products);
  console.log("wtf");
  // console.log(products);
  // console.log(headers);

  return (
    // <CaptionElement />
    <div>
      <MaterialTable
        rowKey={products.id}
        icons={tableIcons}
        title="Edit Products"
        data={products}
        columns={headers}
        // parentChildData={(row, rows) => console.log(rows.find(a))}
        parentChildData={(row, rows) => rows.find((a) => a.id === row.parentId)}
        options={{
          pageSize: 10,
          sorting: true,
          headerStyle: {
            fontSize: "23px",
          },
        }}
        cellEditable={{
          cellStyle: {},
          onCellEditApproved: (newValue, oldValue, rowData, columnDef) => {
            var foundIndex = products.findIndex((i) => i.id === rowData.id);
            if (columnDef.field === "price") {
              newValue = parseFloat(newValue).toFixed(2);

              products[foundIndex].price = newValue;

              let variables = {
                input: {
                  id: rowData.productVariantId.id,
                  price: newValue.toString(),
                },
              };

              updatePrice({ variables });
            }

            if (columnDef.field === "inventoryQuantity") {
              newValue = parseFloat(newValue);

              products[foundIndex].inventoryQuantity = newValue;

              let variables = {
                inventoryAdjustQuantityInput: {
                  inventoryLevelId: rowData.id,
                  availableDelta: newValue - oldValue,
                },
              };
              console.log("update");
              updateQuantity({ variables });
            }

            if (columnDef.field === "publishedAt") {
              //2020-09-17-T00:1600Z

              if (newValue !== "") {
                let time = new Date(newValue);

                // time.setHours(time.getHours() - 7);
                let d = moment(time, "MMM D YYYY, h:mm a").toISOString();
                // console.log(d.toISOString());

                products[foundIndex].publishedAt = newValue;
                let variables = {
                  input: { id: rowData.productId, publishOn: d },
                };
                changeDate({ variables });
              } else {
                let variables = {
                  input: { id: rowData.productId, publishedAt: null },
                };
                changeDate({ variables });
              }
            }

            return new Promise((resolve, reject) => {
              setTimeout(() => {
                if (oldValue) {
                  console.log("setting state");

                  setProducts([...products]);
                }
                resolve();
              }, 600);
            });
          },
        }}
        editable={{
          onRowAdd: (newData) =>
            new Promise((resolve) => {
              setTimeout(() => {
                resolve();
                setProducts((prevState) => {
                  const data = [...prevState];
                  data.push(newData);
                  return [...data];
                });
              }, 600);
            }),
          onRowUpdate: (newData, oldData) =>
            new Promise((resolve) => {
              setTimeout(() => {
                makeRowChanges(newData, oldData);
                if (oldData) {
                  console.log("entered");

                  setProducts((prevState) => {
                    const data = [...prevState];
                    data[data.indexOf(oldData)] = newData;
                    return [...data];
                  });
                  resolve();
                }
              }, 600);
            }),
          onRowDelete: (oldData) =>
            new Promise((resolve) => {
              setTimeout(() => {
                resolve();
                console.log("deleted ", oldData.title);
                console.log("wtf");
                let variables = {
                  input: {
                    // id: oldData.productVariantId.id,
                    id: oldData.productId,
                  },
                };
                // deleteProduct({ variables });

                setProducts((prevState) => {
                  const data = [...prevState].filter((x) => {
                    // return !x.parentId && x.parentId !== oldData.parentId;
                    if (oldData.parentId) {
                      return !x.parentId || (x.parentId && x.id !== oldData.id);
                    } else {
                      return (
                        (!x.parentId && x.id !== oldData.id) ||
                        (x.parentId && x.parentId !== oldData.id)
                      );
                    }

                    // if(!x.parentId && !x.id !== oldData.id){
                    //   return true
                    // }
                  });
                  // data.splice(data.indexOf(oldData), 1);

                  return [...data];
                });
              }, 1500);
            }),
        }}
      />
    </div>
  );
}
